#!/usr/bin/env zsh
# This file is part of arm-sdk
# arm-sdk is Copyright (c) 2016-2021 Ivan J. <parazyd@dyne.org>
#
# This source code is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this source code. If not, see <http://www.gnu.org/licenses/>.
#
# build script for Marvell Kirkwood boards
# Copyright (c) 2020-2021 gryrmln <gryrmln@localhost>

## settings & config
vars+=(device_name device_tree arch size inittab)
vars+=(parted_type dos_boot dos_root bootfs bootopts rootfs rootopts)
vars+=(gitkernel gitbranch)
vars+=(strapdir kerneldir rootfsdir ubootdir)
arrs+=(custmodules)
arrs+=(custom_deb_packages)

device_name="guruplug"
device_tree="kirkwood-guruplug-server-plus"
arch="armel"
size=1920
inittab=("T0:2345:respawn:/sbin/getty -L ttyS0 115200 vt100")

parted_type="dos"
bootfs="vfat"
dos_boot="fat32 8192s 264191s"
rootfs="f2fs"
dos_root="264192s 100%"
rootopts="-q"

# currently (as of 2020/03) on armel
# btrfs performs poorly (as in breaks)
purge_packages+=(btrfs-progs)

# override the unwanted default extra_packages
extra_packages=()
extra_packages=(make gcc)
# Plug Hardware support
# ~ SD filesystem support
extra_packages+=(u-boot-tools usbutils)
extra_packages+=(fdisk dosfstools f2fs-tools)
# ~ mtd\flash filesystem support
extra_packages+=(binutils mtd-utils)
# ~ Bluetooth support
extra_packages+=(bluez bluez-tools)
extra_packages+=(firmware-libertas watchdog)
# ~ Network support
extra_packages+=(nftables)

custmodules=()
custom_deb_packages=(flashybrid_0.23_all.deb)

# To enhance board build versatility, and avoid cross-polluting, enable
# the User to override generic settings with board applicable settings.
# E.g. locate all specific build under ".../tmp/${arch}/${device_name}/..."
# and move all generic build under ".../tmp/${arch}/..."
# Consider Raspberry armel PiI, armhf PiII, armhf, arm64 PiIII, arm64 PiIV
#
# FIXTHEM:
# The board file should be pulled in after all other variables and functions have
# been defined. The board file is pulled in too early, and pulled in twice.

[[ -z "${gitkernel}" ]] && {
    # For default action set
    #gitkernel="mainline"
    # To apply a branch
    gitkernel="git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"
    act "Linux Kernel git source:${gitkernel}"
}

[[ -z "${gitbranch}" ]] && {
    # For default action not acted upon, but must be set
    # Fixed in commit: a232b6f166f46273ce875000ec1a98b2e7fff504
    #gitbranch="master"
    gitbranch="v5.11.1"
    act "Linux Kernel git branch:${gitbranch}"
}

# CLUDGE: chicken, egg, chicken technique to work around two of the following FIXTHEM's
[[ "${rootfsdir}" = "${R}/tmp/${os}-${release}-${version}-${arch}-build/rootfs" ]] || \
    rootfsdir=""
[[ "${ubootdir}" = "${R}/tmp/${os}-${release}-${version}-${arch}-build/u-boot" ]] || \
    ubootdir=""

# FIXTHEM: ${workdir}, ${strapdir}, ${version} and ${arch}
# are defined after this board file is pulled in by 'sdk'.
# Redundancy in path: '-build'
[[ -z "${rootfsdir}" ]] && {
    #rootfsdir="${R}/tmp/${os}-${release}-${version}-${arch}-build/bootstrap"
    # For now set ${rootfsdir} to below what ${workdir} will be.
    rootfsdir="${R}/tmp/${os}-${release}-${version}-${arch}-build/rootfs"
    act "File System Root staging directory:${rootfsdir}"
}

# FIXTHEM: ditto
# Redundancy in path: '-build'
[[ -z "${ubootdir}" ]] && {
    # For now set ${ubootdir} to below what ${workdir} will be.
    ubootdir="${R}/tmp/${os}-${release}-${version}-${arch}-build/u-boot"
    act "Das U-Boot build staging directory:${ubootdir}"
}

# FIXTHEM: ditto
# FIXTHEM: ${kerneldir} is fixed in clone-git()
# as "${R}/tmp/kernels/$device_name/${device_name}-linux"
# Redundancy in path: '${device_name}-'
[[ -z "${kerneldir}" ]] && {
    # Force ${kerneldir} to be the same as that clone-git() uses.
    kerneldir="${R}/tmp/kernels/${device_name}/${device_name}-linux"
    act "Linux Kernel build staging directory:${kerneldir}"
}


plug_compiler_check() {
    fn plug_compiler_check

    # Should the SDK do this ?
    # Quick compiler minimum version check
    gccversion=$(${compiler}gcc -dumpversion)
    autoload is-at-least
    is-at-least 7.3.0 ${gccversion:-0} ||  {
    cat <<EOF

     WARNING: this gcc version ${gccversion} may not be compatible
  with recent U-Boot\\Kernel sources when armel cross compiling!
         gcc version 7.3.0 based compilers consistently
               produced good results during testing

EOF
      ${compiler}gcc --version
      read "?Press [ENTER] to continue..."
    }
}

plug_use_kernelconfig() {
    fn plug_use_kernelconfig
    req=(R device_name kerneldir)
    ckreq || return 1
    # copy a modified kernel configuration file back for future use
    # revert with 'git checkout boards/kernel-configs/${device_name}.config'
    cmp -s $kerneldir/.config \
	${R}/boards/kernel-configs/${device_name}.config || {
	mv ${R}/boards/kernel-configs/${device_name}.config \
		${R}/boards/kernel-configs/${device_name}.config.$(date +%s)
	cp -f $kerneldir/.config \
		${R}/boards/kernel-configs/${device_name}.config
    }
}

# override
prebuild() {
    fn prebuild
    req=(R device_name kerneldir)
    req+=(compiler MAKEOPTS)
    ckreq || return 1

    plug_compiler_check

    notice "Executing $device_name prebuild"

    # FIXME: do the right thing in clone-git!
    #git clean -xdf; git fetch; git checkout ${gitbranch}
    # NOTE: if "$kerneldir" exists but does not contain the correct contents
    # then clone-git (invoked by get-kernel-sources) will do the wrong thing.
    # NOTE: clone-git fixed by commit 7e327d933ba35a157ea87226da341d8d3b05294a
    get-kernel-sources || { zerr; wrapup; }
    pushd $kerneldir/arch/arm/boot/dts || { zerr; wrapup; }
    [[ -n "$device_tree" ]] && {
	if ! grep -q onewire $device_tree.dts 2>&1 >>/dev/null ; then
	    # this will make the kernel build 'dirty'
	    act "patching device tree source"
	    patch -p1 < "${R}/extra/plug/${device_name}/rootfs/usr/local/src/patches/$device_tree.dts.patch" ||
								    { zerr; wrapup; }
	fi
    }
    popd
    pushd $kerneldir
    [[ -n "${nobuildcleanup}" ]] || {
        # Aggressively clean up for the paranoid
        make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
                mrproper || { zerr; wrapup; }
    }
    copy-kernel-config || {
        # fall back to default kernel configuration
        make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
                multi_v5_defconfig || { zerr; wrapup; }
	customisekernel=1
    }
    [[ -z "${customisekernel}" ]] || {
        # manually customise the linux kernel configuration
        make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
		gconfig || { zerr; wrapup; }
	# copy the linux kernel configuration for future plug use
	plug_use_kernelconfig
    }
    popd
}

# override
build_kernel_armel() {
    fn build_kernel_armel
    req=(R device_name kerneldir)
    req+=(compiler MAKEOPTS)
    ckreq || return 1

    notice "Building $device_name kernel"
    prebuild || zerr
    pushd $kerneldir
    make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
            olddefconfig || { zerr; wrapup; }
    # FIXME: do this conditionally and only for .dtb changes
    # remove "-dirty" from kernel version tag
    touch .scmversion
    make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
            zImage dtbs modules || { zerr; wrapup; }
    #make ARCH=arm CROSS_COMPILE=$compiler $MAKEOPTS \
    #    LOADADDR=0x08000 uImage || { zerr; wrapup; }
    popd

    postbuild || zerr
}

# FIXME: Rework to compile under ${workdir}
plug_build_uap() {
    fn plug_build_uap
    req=(R device_name kerneldir)
    req+=(compiler MAKEOPTS)
    req+=(rootfsdir)
    ckreq || return 1

    notice "Building $device_name SD8688 uAP module"
    # NOTE: the source is required on the built target
    # for future kernel upgrades
    pushd "${R}/extra/plug/rootfs/usr/local/src/SD8688-uAP"
    [[ -n "${nobuildcleanup}" ]] || {
	make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
	    KERNELDIR=$kerneldir INSTALL_MOD_PATH=$rootfsdir clean
    }
    make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
        KERNELDIR=$kerneldir INSTALL_MOD_PATH=$rootfsdir
    popd
}

plug_collate_uap() {
    fn plug_collate_uap
    req=(R device_name kerneldir)
    req+=(compiler MAKEOPTS)
    req+=(rootfsdir)
    ckreq || return 1

    notice "Collating $device_name SD8688 uAP module"
    pushd "${R}/extra/plug/rootfs/usr/local/src/SD8688-uAP"
    sudo -E PATH="$PATH" \
	make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
	    KERNELDIR=$kerneldir INSTALL_MOD_PATH=$rootfsdir install || zerr
    [[ -n "${nobuildcleanup}" ]] || {
	# NOTE: not doing this will leave the master rootfs dirty
	make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
	    KERNELDIR=$kerneldir INSTALL_MOD_PATH=$rootfsdir clean
    }
    popd
}

plug_collate_kernel() {
    fn plug_collate_kernel
    req=(R device_name kerneldir)
    req+=(compiler MAKEOPTS)
    req+=(rootfsdir)
    ckreq || return 1

    notice "Collating $device_name linux kernel"
    pushd $kerneldir
    # install kernel modules
    # FIXTHEM: lib/helper:install-kernel-mods() does not let us pass $rootfsdir
    sudo -E PATH="$PATH" \
            make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
            INSTALL_MOD_PATH=$rootfsdir modules_install || { zerr; wrapup; }
            #firmware_install || { zerr; wrapup; }
    sudo mkdir -p $rootfsdir/boot/dtbs
    act "copying kernel files and device-tree blobs"
    sudo cp arch/arm/boot/zImage $rootfsdir/boot/ || zerr
    sudo cp .config $rootfsdir/boot/zImage.cfg || zerr
    sudo cp arch/arm/boot/dts/$device_tree.dtb $rootfsdir/boot/dtbs/ || zerr
    # remove symbolic links to non-existant source on target
    act "cleaning up"
    [ -e "$rootfsdir/lib/modules/*/source" ] &&
	sudo rm -f "$rootfsdir/lib/modules/*/source"
    [ -e "$rootfsdir/lib/modules/*/build" ] &&
	sudo rm -f "$rootfsdir/lib/modules/*/build"
    popd
}

plug_build_u-boot() {
    fn plug_build_u-boot
    req=(R device_name ubootdir)
    req+=(compiler MAKEOPTS)
    ckreq || return 1

    notice "Building $device_name u-boot"
    pushd ${R}/extra/u-boot
    # FIXME: do the right thing here!
    #git clean -xdf; git fetch; git checkout v5.9.1
    # NOTE: U-Boot versions v2019.07 and v2019.10 boards ${device_name}_defconfig
    # are broken for ARMv? < 7 which is most Kirkwood boards.
    # Must also have a (later) U-Boot that matches the compiler toolchain.
    git checkout v2020.01 || { zerr; wrapup; }
    [[ -n "${nobuildcleanup}" ]] || {
        # Aggressively clean up for the paranoid
	make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
		O="$ubootdir" mrproper || { zerr; wrapup; }
    }
    make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler $MAKEOPTS \
            O="$ubootdir" ${device_name}_defconfig || { zerr; wrapup; }
    [[ -z "${customiseuboot}" ]] || {
	    # Optionally customise the u-boot configuration
	    make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
		    O="$ubootdir" gconfig || { zerr; wrapup; }
    }

    #make ARCH=arm CROSS_COMPILE=$compiler $MAKEOPTS \
    #        O="$ubootdir" olddefconfig || { zerr; wrapup; }

    # Don't bother with u-boot.dtb, it can lag that provided by the kernel
    make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
            O="$ubootdir" u-boot.bin u-boot.kwb u-boot.elf u-boot.cfg || { zerr; return 1; }
    popd
}

plug_collate_u-boot() {
    fn plug_collate_u-boot
    req=(R device_name rootfsdir ubootdir)
    ckreq || return 1

    notice "Collating $device_name u-boot"
    sudo mkdir -p $rootfsdir/boot
    act "copying u-boot files and device-tree blob"
    for sfx in "-nodtb.bin" "-dtb.bin" ".kwb" ".elf" ".cfg" ".map" ".lds" ; do
        sudo cp $ubootdir/u-boot$sfx $rootfsdir/boot/ || zerr
    done
}

plug_oemlinux_script() {
    fn plug_oem_script
    req=(device_name rootfsdir)
    ckreq || return 1

    # legacy u-boot and oem legacy linux
    act "creating $device_name oem legacy kernel image u-boot script"
    cat <<EOF | sudo tee ${rootfsdir}/boot/oemlinux.ubt >>/dev/null
setenv machid_sheevaplug '0x0831'
setenv machid_guruplug '0x0a63'
setenv machid_esheevaplug '0x0a76'
setenv machid_dreamplug '0x0dde'
setenv machid \${machid_${device_name}}
setenv bootargs 'console=ttyS0,115200n8 root=/dev/sda2 rootfstype=ext3 rootwait earlyprintk';
setenv bootcmd 'fatload usb 0:1 0x6400000 uImage; bootm 0x6400000;'
EOF
    # legacy image format
    sudo mkimage -A arm -O linux -T script -C none \
            -a 0 -e 0 \
            -n Script-${device_name}-oemlinux \
            -d $rootfsdir/boot/oemlinux.ubt \
            $rootfsdir/boot/oemlinux.ubs >>/dev/null || zerr
}

plug_bootenv_script() {
    fn plug_bootenv_script
    req=(device_name rootfsdir)
    ckreq || return 1

    # u-boot bootcmd environment
    act "creating $device_name u-boot bootenv utility script"
    cat <<EOF | sudo tee ${rootfsdir}/boot/bootenv.ubt >>/dev/null
setenv bootcmd_bak \${bootcmd}
setenv bootcmd 'usb start; fatload usb 1:1 0x2000000 boot.ubs; source 0x2000000;'
saveenv
EOF
    # legacy image format
    sudo mkimage -A arm -O linux -T script -C none \
            -a 0 -e 0 \
            -n Script-${device_name}-bootenv \
            -d $rootfsdir/boot/bootenv.ubt \
            $rootfsdir/boot/bootenv.ubs >>/dev/null || zerr
}

plug_image_uimage() {
    fn plug_image_uimage
    req=(device_name rootfsdir)
    ckreq || return 1

    # u-boot device-tree and linux device-tree
    # or u-boot legacy (non-device-tree) and linux legacy (non-device-tree)
    act "creating $device_name kernel u-boot image"
    # legacy image format
    sudo mkimage -A arm -O linux -T kernel -C none \
            -a 0x00008000 -e 0x00008000 \
            -n Linux-${device_name} \
            -d $rootfsdir/boot/zImage \
            $rootfsdir/boot/zImage.ubi >>/dev/null || zerr
}

plug_image_script() {
    fn plug_image_script
    req=(device_name rootfsdir)
    ckreq || return 1

    # u-boot device-tree and linux device-tree
    act "creating $device_name kernel image and dtb u-boot script"
    cat <<EOF | sudo tee ${rootfsdir}/boot/zImage-dtb.ubt >>/dev/null
setenv bootargs 'console=ttyS0,115200n8 root=/dev/sdb2 rootfstype=${rootfs} rootwait earlyprintk';
fatload usb 1:1 0x6000000 zImage.ubi;
fatload usb 1:1 0x6E00000 dtbs/${device_tree}.dtb;
bootm 0x6000000 - 0x6E00000;
EOF
    # legacy image format
    sudo mkimage -A arm -O linux -T script -C none \
            -a 0 -e 0 \
            -n Script-${device_name}-dtb \
            -d $rootfsdir/boot/zImage-dtb.ubt \
            $rootfsdir/boot/zImage-dtb.ubs >>/dev/null || zerr
}

plug_legacy_uimage() {
    fn plug_legacy_uimage_script
    req=(device_name rootfsdir)
    ckreq || return 1

    # u-boot legacy (non-device-tree) with linux device-tree
    act "creating $device_name device-tree appended kernel legacy u-boot image"
    sudo cat $rootfsdir/boot/zImage $rootfsdir/boot/dtbs/$device_tree.dtb | sudo tee $rootfsdir/boot/zImage-dtl >/dev/null
    # legacy image format
    sudo mkimage -A arm -O linux -T kernel -C none \
            -a 0x00008000 -e 0x00008000 \
            -n Linux-${device_name}-dtl \
            -d $rootfsdir/boot/zImage-dtl \
            $rootfsdir/boot/zImage-dtl.ubi >>/dev/null || zerr
}

plug_legacy_script() {
    fn plug_legacy_script
    req=(device_name rootfsdir)
    ckreq || return 1

    # u-boot legacy (non-device-tree) with linux device-tree
    act "creating $device_name device-tree appended kernel legacy u-boot image script"
    cat <<EOF | sudo tee ${rootfsdir}/boot/zImage-dtl.ubt >>/dev/null
setenv bootargs 'console=ttyS0,115200n8 root=/dev/sdb2 rootfstype=${rootfs} rootwait earlyprintk';
fatload usb 1:1 0x6400000 zImage-dtl.ubi;
bootm 0x6400000;
EOF
    # legacy image format
    sudo mkimage -A arm -O linux -T script -C none \
            -a 0 -e 0 \
            -n Script-${device_name}-dtl \
            -d $rootfsdir/boot/zImage-dtl.ubt \
            $rootfsdir/boot/zImage-dtl.ubs >>/dev/null || zerr
}

# override
postbuild() {
    fn postbuild
    req=(R device_name kerneldir rootfsdir bootfs)
    req+=(compiler MAKEOPTS)
    ckreq || return 1

    notice "Executing $device_name postbuild"

    # optionally suppress Marvell uAP build
    [[ -n "${nouap}" ]] || plug_build_uap

    # optionally suppress Das U-Boot build
    [[ -n "${nouboot}" ]] || plug_build_u-boot

    # FIXTHEM: copy-root-overlay
    # Do this instead, destination "${rootfsdir}/"
    sudo mkdir -p "${rootfsdir}/"
    # Nothing good in generic-root
    #[[ -d ${R}/extra/generic-root ]] && \
    #        notice "Copying generic-root" && \
    #        sudo cp -rf ${R}/extra/generic-root/* $rootfsdir
    # override any fstab from generic-root
    # FIXME: fstab entries, u-boot scripts, and kernel cmdline should be synced
    sudo mkdir -p "${rootfsdir}/etc"
    # External SD
    cat <<EOF | sudo tee ${rootfsdir}/etc/fstab >>/dev/null
## <file system>        <mount point> <type> <options>                <dump> <pass>
/dev/sdb1                /boot                $bootfs        defaults                0    1
EOF
    # optionally add\override from plug root
    [[ -d ${R}/extra/plug/rootfs ]] && \
            notice "Copying plug rootfs" && \
            sudo cp -rf --no-preserve=ownership ${R}/extra/plug/rootfs/* $rootfsdir

    # optionally add\override from custom device specific root
    [[ -d "${R}/extra/plug/${device_name}/rootfs" ]] && \
            notice "Copying custom ${device_name} rootfs" && \
            sudo cp -rf --no-preserve=ownership ${R}/extra/plug/$device_name/rootfs/* $rootfsdir

    # do these after to ensure that anything generated
    # 'version specific' overrides anything generic-root or custom
    plug_collate_kernel
    # optionally suppress Marvell uAP build
    [[ -n "${nouap}" ]] || plug_collate_uap
    # optionally suppress Das U-Boot build
    [[ -n "${nouboot}" ]] || plug_collate_u-boot
    plug_oemlinux_script
    plug_bootenv_script
    plug_image_uimage
    plug_image_script
    plug_legacy_uimage
    plug_legacy_script

    # copy the desired boot script to be the default boot script file
    # U-Boot legacy with Linux FDT u-boot script 
    #sudo cp -rf ${rootfsdir}/boot/zImage-dtl.ubs ${rootfsdir}/boot/boot.ubs
    # U-Boot FDT with Linux FDT u-boot script
    sudo cp -rf ${rootfsdir}/boot/zImage-dtb.ubs ${rootfsdir}/boot/boot.ubs

    # optionally suppress build cleanup
    [[ -n "${nobuildcleanup}" ]] || {
        pushd "${R}/extra/u-boot"
        make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
                clean || { zerr; wrapup; }
        popd

        pushd "${kerneldir}"
        # this destroys 'modules.order' required during modules_install
        # is it a kernel build bug ?!?
        make $MAKEOPTS ARCH=arm CROSS_COMPILE=$compiler \
                clean || { zerr; wrapup; }
        popd
    }
    postbuild-clean
}

# override
image_prepare_raw()
{
	fn image_prepare_raw
	req=(workdir size image_name)
	ckreq || return 1

	notice "Creating 0xFF filled raw image of $size MB"
	touch "${workdir}/${image_name}.img"
	chattr -f +C "${workdir}/${image_name}.img"
	dd if=/dev/zero bs=1M count="$size" 2>>/dev/null | tr '\000' '\377' \
		| sudo dd of="${workdir}/${image_name}.img" 2>>/dev/null || { zerr; return 1; }
}

# override
strapdir_to_image()
{
	fn strapdir_to_image
	req=(R workdir strapdir rootfsdir image_name)
	ckreq || return 1

	notice "Copying strapdir to image ..."

	if [[ ! -d "${workdir}/mnt" ]]; then
		die "${workdir}/mnt doesn't exist. Did you run image_mount?"
		zerr; return 1
	fi

	pushd "${strapdir}"
	sudo find . \
		-not -path "./dev/*" \
		-a -not -path "./proc/*" \
		-a -not -path "./sys/*" \
		| sudo cpio -adm -p "${workdir}/mnt" 2>/dev/null || { zerr; return 1; }
	popd
	[[ "${rootfsdir}" == "${strapdir}" ]] || {
		notice "Copying rootfsdir to image ..."
		pushd "${rootfsdir}"
		sudo find . \
			-not -path "./dev/*" \
			-a -not -path "./proc/*" \
			-a -not -path "./sys/*" \
			| sudo cpio -adm -p "${workdir}/mnt" 2>/dev/null || { zerr; return 1; }
		popd
	}

	# A tar archive can be preferable over an image,
	# manually partitioning the flash media lends to:
	# : maximise usage of available flash
	# : partitions can be sized for the flash erase blocksize
	# : the rootfs filesystem format can be changed
	# : add filesystem encryption
	# : multiple root filesystems, e.g. recovery partition
	# : a compressed tar archive is smaller
	# : faster and less flash (wear) writing
	#
	# now create the tar from the completed rootfs image
	notice "Creating filesystem tar'chive"
	mkdir -p "${R}/dist"
	sudo chmod o+rX "${workdir}/mnt"
	pushd "${workdir}/mnt"
		# FIXME: chmod to 'user'
		touch "${R}/dist/${image_name}.txz"
		sudo tar -cJf "${R}/dist/${image_name}.txz" . || { zerr; return 1; }
	popd
        act "Calculating sha256 checksum"
	sha256sum "${R}/dist/${image_name}.txz" > "${R}/dist/${image_name}.txz.sha256"
}

# depreciated
wrapup()
{
	exit 255
}
