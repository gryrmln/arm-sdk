
WARNING: recent U-Boot\Kernel sources may require recent armel cross compilers.

At the date of testing:- June 2019
Gcc version 7.1.1 based compiler toolchains produced binaries that would hang.
Gcc version 7.4.0 based compiler toolchains consistently produced good results
with recent U-Boot\Kernel sources. Older U-Boot\Kernel sources would load but
fail when accessing key hardware components.


Compiler version
~~~~~~~~~~~~~~~~

boot]# egrep -iao "gcc.*" u-boot.kwb
gcc (Linaro GCC 7.4-2019.02) 7.4.1 20181213 [linaro-7.4-2019.02 revision 56ec6f6b99cc167ff0c2f8e1a2eed33b1edc85d4]

boot]# grep gcc zImage.cfg
# Compiler: arm-linux-gnueabi-gcc (Linaro GCC 7.4-2019.02) 7.4.1 20181213 [linaro-7.4-2019.02 revision 56ec6f6b99cc167ff0c2f8e1a2eed33b1edc85d4]


U-Boot & Linux Kernel source versions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

u-boot]$ git describe
v2019.04

dreamplug-linux]$ git describe
v5.1.12


Running OEM legacy Non-DT kernels on recent DT U-Boot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Set a 'machid' environment variable in U-Boot to match the value expected
by the legacy kernel.

setenv machid '0x0a63'

NOTE: due to Globalscale Technologies failure to follow the Linux Kernel
open source development proceedures, the machine id used in the OEM legacy
kernels often lagged the hardware the kernel was meant to run on.

E.g. Globalscale Technologies built kernels for early DreamPlug's used it's
predecessor GuruPlug 'machid'. The GuruPlug used the SheevaPlug's 'machid'.

SHEEVAPLUG		'0x0831'
GURUPLUG		'0x0a63'
ESATA_SHEEVAPLUG	'0x0a76'
DREAMPLUG		'0x0dde'


Filename Extensions
~~~~~~~~~~~~~~~~~~~
The following convention for filename extensions has been adopted.


System files:
.ubs		Das U-Boot script (RAM runable via 'source' command).
.ubi		Das U-Boot image (RAM executable via 'bootm').
.dtb		Device Tree Blob.
.dto		Device Tree Overlay Blob.


Accessory files:
.ubt		Das U-Boot script source text.
.bin		Binary (flashable with openocd).
.elf		Binary (RAM loadable and executable via Das U-Boot).
.kwb		Kirkwood SoC binary (kwboot UART transfer RAM bootable).
.cfg		Configuration (kconfig build configuration text).
.dts		Device Tree Source.


Files
~~~~~

System files:
boot.ubs		Das U-Boot script to be executed at boot by default.
			(copy of either 'zImage-dtb.ubs' or 'zImage-dtl.ubs').

zImage.ubi		Das U-Boot Linux Kernel image.
dtbs/*.dtb		Device Tree Blobs.
zImage-dtb.ubs		Das U-Boot Linux Kernel and Device Tree script.

zImage-dtl.ubi		Das U-Boot Linux Kernel with Device Tree appended image.
zImage-dtl.ubs		Das U-Boot Linux Kernel with Device Tree appended script.


Accessory files:
zImage.cfg		Linux Kernel Kconfig text.
zImage			Linux Kernel.
zImage-dtl		Linux Kernel with Device Tree appended.
zImage-dtl.ubt		Das U-Boot Linux Kernel with Device Tree appended script source text.
zImage-dtb.ubt		Das U-Boot Linux Kernel and Device Tree script source text.


u-boot.cfg		Das U-Boot Kconfig text.
u-boot-nodtb.bin	Das U-Boot (no appended Device Tree).
u-boot-dtb.bin		Das U-Boot with Device Tree appended.
u-boot.kwb		Das U-Boot Kirkwood Boot.
u-boot.elf		Das U-Boot Executable Linkable Format.

u-boot.lds		Das U-Boot linker text
u-boot.map		Das U-Boot linker symbols text
