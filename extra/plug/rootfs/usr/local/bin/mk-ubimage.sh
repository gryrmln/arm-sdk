#!/bin/bash
# vim:ai:sw=4
BOOTIMAGE=${1%%.*}
[ -f "${BOOTIMAGE}" ] || {
    echo "No Image \"${BOOTIMAGE}\""
    exit -1
}
MKIMAGE=$(which mkimage) || {
    echo "No mkimage executable in path"
    exit -1
}
${MKIMAGE} -A arm -O linux -a 0 -e 0 -T kernel -C none \
    -a 0x00008000 -e 0x00008000  \
    -n "U-Boot Image"  -d "${BOOTIMAGE}" "${BOOTIMAGE}.ubi"
echo
ls -l "${BOOTIMAGE}"
