#!/bin/bash
# vim:ai:sw=4
BOOTSCRIPT=${1%%.*}
[ -f "${BOOTSCRIPT}.ubt" ] || {
    echo "No U-Boot Text \"${BOOTSCRIPT}.ubt\""
    exit -1
}
MKIMAGE=$(which mkimage) || {
    echo "No mkimage executable in path"
    exit -1
}
${MKIMAGE} -A arm -O linux -a 0 -e 0 -T script -C none \
    -n "U-Boot Script"  -d "${BOOTSCRIPT}.ubt" "${BOOTSCRIPT}.ubs"
echo
ls -l "${BOOTSCRIPT}".*
