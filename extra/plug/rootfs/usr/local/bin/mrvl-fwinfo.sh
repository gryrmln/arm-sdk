#!/bin/bash
# vim:ai:sw=2
if [ $# -eq 0 ] ; then
  for F in *.bin ; do
    echo -e "~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo -e "FW $F"
    strings $F | egrep '^RC:|\<yhou|\$Id'
    echo -e "\n"
  done
else
  echo -e "~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  echo -e "FW $@"
  strings $@ | egrep '^RC:|\<yhou|\$Id'
  echo -e "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~"
fi
