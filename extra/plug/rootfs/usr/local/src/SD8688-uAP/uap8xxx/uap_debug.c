/** @file uap_debug.c
  * @brief This file contains functions for debug proc file.
  *
  * Copyright (C) 2008-2009, Marvell International Ltd.
  *
  * This software file (the "File") is distributed by Marvell International
  * Ltd. under the terms of the GNU General Public License Version 2, June 1991
  * (the "License").  You may use, redistribute and/or modify this File in
  * accordance with the terms and conditions of the License, a copy of which
  * is available along with the File in the gpl.txt file or by writing to
  * the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
  * 02111-1307 or on the worldwide web at http://www.gnu.org/licenses/gpl.txt.
  *
  * THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE
  * ARE EXPRESSLY DISCLAIMED.  The License provides additional details about
  * this warranty disclaimer.
  *
  */
#ifdef CONFIG_PROC_FS
#include  "uap_headers.h"

/********************************************************
		Local Variables
********************************************************/

#define item_size(n) (sizeof ((uap_adapter *)0)->n)
#define item_addr(n) ((u32) &((uap_adapter *)0)->n)

#define item_dbg_size(n) (sizeof (((uap_adapter *)0)->dbg.n))
#define item_dbg_addr(n) ((u32) &(((uap_adapter *)0)->dbg.n))

#define item_dev_size(n) (sizeof ((uap_dev_t *)0)->n)
#define item_dev_addr(n) ((u32) &((uap_dev_t *)0)->n)

/** MicroAp device offset */
#define OFFSET_UAP_WIFI		0x01
/** Bluetooth adapter offset */
#define OFFSET_UAP_BT	0x02

struct debug_data
{
    /** Name */
    char name[32];
    /** Size */
    u32 size;
    /** Address */
    u32 addr;
    /** Offset */
    u32 offset;
    /** Flag */
    u32 flag;
};

/* To debug any member of uap_adapter, simply add one line here. */
static struct debug_data items[] = {
    {"cmd_sent", item_dev_size(cmd_sent), 0, item_dev_addr(cmd_sent),
     OFFSET_UAP_WIFI},
    {"data_sent", item_dev_size(data_sent), 0, item_dev_addr(data_sent),
     OFFSET_UAP_WIFI},
    {"IntCounter", item_size(IntCounter), 0, item_addr(IntCounter),
     OFFSET_UAP_BT},
    {"cmd_pending", item_size(cmd_pending), 0, item_addr(cmd_pending),
     OFFSET_UAP_BT},
    {"num_cmd_h2c_fail", item_dbg_size(num_cmd_host_to_card_failure), 0,
     item_dbg_addr(num_cmd_host_to_card_failure), OFFSET_UAP_BT},
    {"num_tx_h2c_fail", item_dbg_size(num_tx_host_to_card_failure), 0,
     item_dbg_addr(num_tx_host_to_card_failure), OFFSET_UAP_BT},
    {"psmode", item_size(psmode), 0, item_addr(psmode), OFFSET_UAP_BT},
    {"ps_state", item_size(ps_state), 0, item_addr(ps_state),
     OFFSET_UAP_BT},
#ifdef DEBUG_LEVEL1
    {"drvdbg", sizeof(drvdbg), (u32) & drvdbg, 0, 0}
#endif
};

static int num_of_items = sizeof(items) / sizeof(items[0]);

/********************************************************
		Global Variables
********************************************************/

/********************************************************
		Local Functions
********************************************************/

static int proc_open_debug(struct inode *inode, struct file *file);
static ssize_t proc_read_debug(struct file *file,
                         char __user * buffer, size_t len, loff_t * offset);
static ssize_t proc_write_debug(struct file *file,
                          const char __user * buffer,
                          size_t len, loff_t * offset);
static int proc_close_debug(struct inode *inode, struct file *file);

static const struct proc_ops proc_ops_debug = {
        //.owner = THIS_MODULE,
        .proc_read = proc_read_debug,
        .proc_write = proc_write_debug,
        .proc_open = proc_open_debug,
        .proc_lseek = default_llseek,
        .proc_release = proc_close_debug,
};

/*
struct proc_debug {
	struct debug_data *items;
};
*/

/**
 *  @brief proc read function
 *
 *  @param file    pointer to data
 *  @param buffer  pointer to buffer
 *  @param len     length
 *  @param offset  offset
 *  @return 	   count of data written out
 */
static ssize_t proc_read_debug(struct file *file,
                         char __user * buffer, size_t len, loff_t * offset)
{
        //struct proc_debug *priv = file->private_data;
	//struct debug_data *d = priv->items;
	struct debug_data *d = file->private_data;
	char *lb, *p;
	int val;
	int i;
	ssize_t ret;

	if (MODULE_GET == 0)
	    return UAP_STATUS_FAILURE;

        if (!d) {
		ret = -EINVAL;
		goto fault;
        }

        /* sizeof(items) should always end up being a little
	 * larger than it's human readable text equivalent */
	if ((lb = kmalloc(sizeof(items), GFP_KERNEL)) == NULL) {
		ret = -ENOMEM;
		goto fault;
	}
        p = lb;

	for (i = 0; i < num_of_items; i++) {
	    if (d[i].size == 1)
		val = *((u8 *) d[i].addr);
	    else if (d[i].size == 2)
		val = *((u16 *) d[i].addr);
	    else if (d[i].size == 4)
		val = *((u32 *) d[i].addr);
            else
		val = 0;
	    p += sprintf(p, "%s=%d\n", d[i].name, val);
	}
	ret = simple_read_from_buffer(buffer, len, offset, lb, strlen(lb));
	kfree(lb);
fault:
	MODULE_PUT;
	return ret;
}

/**
 *  @brief proc write function
 *
 *  @param file    pointer to data
 *  @param buffer  pointer to buffer
 *  @param len     length
 *  @param offset  offset
 *  @return 	   count of data read in
 */
static ssize_t proc_write_debug(struct file *file,
                          const char __user * buffer,
                          size_t len, loff_t * offset)
{
        ssize_t ret = 0;
        //struct proc_debug *priv = file->private_data;
	//struct debug_data *d = priv->items;
        struct debug_data *d = file->private_data;
        int cnt = 2048;

    int r, i;
    char *pdata;
    char *p;
    char *p0;
    char *p1;
    char *p2;

	/*
        if (ret > 0)
                priv->writelen = max_t(int, priv->writelen, *offset);
	*/

    if (MODULE_GET == 0) {
        ret = UAP_STATUS_FAILURE;
	goto fault;
    }

    pdata = (char *) kmalloc(cnt, GFP_KERNEL);
    if (pdata == NULL) {
	ret = -ENOMEM;
	goto fault;
    }

    ret = simple_write_to_buffer(pdata, cnt, offset, buffer, len);

    /*
    if (copy_from_user(pdata, buf, cnt)) {
        PRINTM(INFO, "Copy from user failed\n");
        kfree(pdata);
        MODULE_PUT;
        return 0;
    }
    */

    p0 = pdata;
    for (i = 0; i < num_of_items; i++) {
        do {
            p = strstr(p0, d[i].name);
            if (p == NULL)
                break;
            p1 = strchr(p, '\n');
            if (p1 == NULL)
                break;
            p0 = p1++;
            p2 = strchr(p, '=');
            if (!p2)
                break;
            p2++;
            r = string_to_number(p2);
            if (d[i].size == 1)
                *((u8 *) d[i].addr) = (u8) r;
            else if (d[i].size == 2)
                *((u16 *) d[i].addr) = (u16) r;
            else if (d[i].size == 4)
                *((u32 *) d[i].addr) = (u32) r;
            break;
        } while (TRUE);
    }
    kfree(pdata);
#ifdef DEBUG_LEVEL1
    printk(KERN_ALERT "drvdbg = 0x%x\n", drvdbg);
    printk(KERN_ALERT "INFO  (%08lx) %s\n", DBG_INFO,
           (drvdbg & DBG_INFO) ? "X" : "");
    printk(KERN_ALERT "WARN  (%08lx) %s\n", DBG_WARN,
           (drvdbg & DBG_WARN) ? "X" : "");
    printk(KERN_ALERT "ENTRY (%08lx) %s\n", DBG_ENTRY,
           (drvdbg & DBG_ENTRY) ? "X" : "");
    printk(KERN_ALERT "CMD_D (%08lx) %s\n", DBG_CMD_D,
           (drvdbg & DBG_CMD_D) ? "X" : "");
    printk(KERN_ALERT "DAT_D (%08lx) %s\n", DBG_DAT_D,
           (drvdbg & DBG_DAT_D) ? "X" : "");
    printk(KERN_ALERT "CMND  (%08lx) %s\n", DBG_CMND,
           (drvdbg & DBG_CMND) ? "X" : "");
    printk(KERN_ALERT "DATA  (%08lx) %s\n", DBG_DATA,
           (drvdbg & DBG_DATA) ? "X" : "");
    printk(KERN_ALERT "ERROR (%08lx) %s\n", DBG_ERROR,
           (drvdbg & DBG_ERROR) ? "X" : "");
    printk(KERN_ALERT "FATAL (%08lx) %s\n", DBG_FATAL,
           (drvdbg & DBG_FATAL) ? "X" : "");
    printk(KERN_ALERT "MSG   (%08lx) %s\n", DBG_MSG,
           (drvdbg & DBG_MSG) ? "X" : "");
#endif
fault:
    MODULE_PUT;
    return ret;
}

static int proc_open_debug(struct inode *inode, struct file *file)
{
	/*
        struct proc_debug *priv;

        if ((file->private_data =
             kzalloc(sizeof(struct proc_debug), GFP_KERNEL)) == NULL)
                return -ENOMEM;
        priv = file->private_data;
        priv->items = &items[0];
	*/
        file->private_data = &items[0];
        return 0;
}

static int proc_close_debug(struct inode *inode, struct file *file)
{
	/*
        struct proc_debug *priv = file->private_data;

        kfree(priv);
	*/
        return 0;
}

/********************************************************
		Global Functions
********************************************************/
/**
 *  @brief create debug proc file
 *
 *  @param priv	   pointer uap_private
 *  @param dev     pointer net_device
 *  @return 	   N/A
 */
void
uap_debug_entry(uap_private * priv, struct net_device *dev)
{
    int i;
    struct proc_dir_entry *pde_debug;
    //struct debug_data *data;

    /* need the upper level /proc/uap/uap? entry */
    if (priv->proc_entry == NULL)
        return;

    /* create /proc/uap/uap?/debug */
    pde_debug = proc_create_data("debug", 0644,
			 priv->proc_entry, &proc_ops_debug, NULL);

    if (pde_debug == NULL)
        return;

    /* patch up the debug table */
    for (i = 0; i < num_of_items; i++) {
        if (items[i].flag & OFFSET_UAP_BT)
            items[i].addr = items[i].offset + (u32) priv->adapter;
        if (items[i].flag & OFFSET_UAP_WIFI)
            items[i].addr = items[i].offset + (u32) & priv->uap_dev;
    }

/*
    entry->data = &items[0];
    entry->read_proc = uap_debug_read;
    entry->write_proc = uap_debug_write;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,30)
    entry->owner = THIS_MODULE;
#endif
*/
}

/**
 *  @brief remove proc file
 *
 *  @param priv	   pointer uap_private
 *  @return 	   N/A
 */
void
uap_debug_remove(uap_private * priv)
{
    remove_proc_subtree("debug", priv->proc_entry);
}

#endif
