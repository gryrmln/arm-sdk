/** @file uap_proc.c
  * @brief This file contains functions for proc file.
  *
  * Copyright (C) 2008-2009, Marvell International Ltd.
  *
  * This software file (the "File") is distributed by Marvell International
  * Ltd. under the terms of the GNU General Public License Version 2, June 1991
  * (the "License").  You may use, redistribute and/or modify this File in
  * accordance with the terms and conditions of the License, a copy of which
  * is available along with the File in the gpl.txt file or by writing to
  * the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
  * 02111-1307 or on the worldwide web at http://www.gnu.org/licenses/gpl.txt.
  *
  * THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE
  * ARE EXPRESSLY DISCLAIMED.  The License provides additional details about
  * this warranty disclaimer.
  *
  */
#ifdef CONFIG_PROC_FS
#include "uap_headers.h"

/** /proc directory root */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,26)
#define PROC_DIR NULL
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,24)
#define PROC_DIR &proc_root
#else
#define PROC_DIR proc_net
#endif

//static kuid_t proc_kuid;
//static int proc_uid /* = 0 */;

//static kgid_t proc_kgid;
//static int proc_gid /* = 0 */;

/********************************************************
		Local Variables
********************************************************/

/********************************************************
		Global Variables
********************************************************/

/********************************************************
		Local Functions
********************************************************/

/* generic procfs operations */
/* NOTUSED
static int proc_open_uap(struct inode *inode, struct file *file);
*/
static ssize_t proc_read_uap(struct file *file,
			 char __user * buffer, size_t len, loff_t * offset);
static ssize_t proc_write_uap(struct file *file,
			  const char __user * buffer,
			  size_t len, loff_t * offset);
static int proc_release_uap(struct inode *inode, struct file *file);

/* uap procfs operations */
static int proc_open_info(struct inode *inode, struct file *file);

static int proc_open_hwstatus(struct inode *inode, struct file *file);
static int proc_release_hwstatus(struct inode *inode, struct file *file);

/* NOTUSED
static const struct proc_ops proc_ops_generic = {
	//.owner = THIS_MODULE,
	.proc_read = proc_read_uap,
	.proc_write = proc_write_uap,
	.proc_open = proc_open_uap,
	.proc_lseek = default_llseek,
	.proc_release = proc_release_uap,
};
*/

static const struct proc_ops proc_info_ops = {
	//.owner = THIS_MODULE,
	.proc_open = proc_open_info,
	.proc_read = proc_read_uap,
	.proc_write = proc_write_uap,
	.proc_lseek = default_llseek,
	.proc_release = proc_release_uap,
};

static const struct proc_ops proc_hwstatus_ops = {
	//.owner = THIS_MODULE,
	.proc_open = proc_open_hwstatus,
	//.read           = uap_hwstatus_read,
	.proc_read = proc_read_uap,
	//.write          = uap_hwstatus_write,
	.proc_write = proc_write_uap,
	.proc_lseek = default_llseek,
	.proc_release = proc_release_hwstatus,
};

/**
 *  @brief convert string to number
 *
 *  @param s   	   pointer to numbered string
 *  @return 	   converted number from string s
 */
int string_to_number(char *s)
{
	int r = 0;
	int base = 0;
	int pn = 1;

	if (strncmp(s, "-", 1) == 0) {
		pn = -1;
		s++;
	}
	if ((strncmp(s, "0x", 2) == 0) || (strncmp(s, "0X", 2) == 0)) {
		base = 16;
		s += 2;
	} else
		base = 10;

	for (s = s; *s != 0; s++) {
		if ((*s >= '0') && (*s <= '9'))
			r = (r * base) + (*s - '0');
		else if ((*s >= 'A') && (*s <= 'F'))
			r = (r * base) + (*s - 'A' + 10);
		else if ((*s >= 'a') && (*s <= 'f'))
			r = (r * base) + (*s - 'a' + 10);
		else
			break;
	}

	return (r * pn);
}

/*
 *  What we want from the proc_fs is to be able to efficiently read
 *  and write the configuration.  To do this, we want to read the
 *  configuration when the file is opened and write it when the file is
 *  closed.  So basically we allocate a read buffer at open and fill it
 *  with data, and allocate a write buffer and read it at close.
 *
 *  The read routine is generic, it relies on the preallocated rbuffer
 *  to supply the data.
 *
 *  The write routine is generic, it fills in a preallocated wbuffer
 *  with the supplied data.
 */

struct proc_data {
	int release_buffer;
	int readlen;
	char *rbuffer;
	int writelen;
	int maxwritelen;
	char *wbuffer;
	void (*on_close) (struct inode *, struct file *);
};

static ssize_t proc_read_uap(struct file *file,
			 char __user * buffer, size_t len, loff_t * offset)
{
	struct proc_data *priv = file->private_data;

	if (!priv->rbuffer)
		return -EINVAL;

	return simple_read_from_buffer(buffer, len, offset, priv->rbuffer,
				       priv->readlen);
}

static ssize_t proc_write_uap(struct file *file,
			  const char __user * buffer,
			  size_t len, loff_t * offset)
{
	ssize_t ret;
	struct proc_data *priv = file->private_data;

	if (!priv->wbuffer)
		return -EINVAL;

	ret = simple_write_to_buffer(priv->wbuffer, priv->maxwritelen, offset,
				     buffer, len);
	if (ret > 0)
		priv->writelen = max_t(int, priv->writelen, *offset);

	return ret;
}

/* NOTUSED
static int proc_open_uap(struct inode *inode, struct file *file)
{
	struct proc_data *data;

	if ((file->private_data =
	     kzalloc(sizeof(struct proc_data), GFP_KERNEL)) == NULL)
		return -ENOMEM;
	data = file->private_data;
	if ((data->rbuffer = kmalloc(2048, GFP_KERNEL)) == NULL) {
		kfree(file->private_data);
		return -ENOMEM;
	}
	if ((data->wbuffer = kmalloc(2048, GFP_KERNEL)) == NULL) {
		kfree(data->rbuffer);
		kfree(file->private_data);
		return -ENOMEM;
	}
	data->maxwritelen = sizeof(data->wbuffer);
	return 0;
}
*/

static int proc_release_uap(struct inode *inode, struct file *file)
{
	struct proc_data *data = file->private_data;

	if (data->on_close != NULL)
		data->on_close(inode, file);
	kfree(data->rbuffer);
	kfree(data->wbuffer);
	kfree(data);
	return 0;
}

/**
 *  @brief hwstatus proc read function
 *
 *  @param page	   pointer to buffer
 *  @param s       read data starting position
 *  @param off     offset
 *  @param cnt     counter
 *  @param eof     end of file flag
 *  @param data    data to output
 *  @return 	   number of output data
 */
/*
static int
uap_hwstatus_read(char *page, char **s, off_t off, int cnt, int *eof,
                  void *data)
{
    char *p = page;
    struct net_device *netdev = data;
    uap_private *priv = (uap_private *) netdev_priv(netdev);
    MODULE_GET;
    p += sprintf(p, "%d\n", priv->adapter->HardwareStatus);
    MODULE_PUT;
    return p - page;
}
*/

/**
 *  @brief proc_open_info
 *
 *  @param inode
 *  @param file
 *  @return 0 or -ERROR
 */
static int proc_open_info(struct inode *inode, struct file *file)
{
	//char fmt[64];
	char *p;
	int i;
	struct proc_data *data;

	struct netdev_hw_addr *ha;
	struct net_device *dev = PDE_DATA(inode);
	uap_private *priv = (uap_private *) netdev_priv(dev);

	if ((file->private_data =
	     kzalloc(sizeof(struct proc_data), GFP_KERNEL)) == NULL)
		return -ENOMEM;
	data = file->private_data;
	if ((data->rbuffer = kmalloc(2048, GFP_KERNEL)) == NULL) {
		kfree(file->private_data);
		return -ENOMEM;
	}
	data->maxwritelen = 2048;

	p = data->rbuffer;

	//strncpy(fmt, DRIVER_VERSION, sizeof(fmt) - 1);

	netif_addr_lock_bh(dev);

	p += sprintf(p, "driver_name = " "\"uap\"\n");
	//p += sprintf(p, "driver_version = %s-(FP%s)", fmt, FPNUM);
	p += sprintf(p, "driver_version = %s-(FP%s)", DRIVER_VERSION, FPNUM);
	p += sprintf(p, "\nInterfaceName=\"%s\"\n", dev->name);
	p += sprintf(p, "State=\"%s\"\n",
		     ((priv->MediaConnected ==
		       FALSE) ? "Disconnected" : "Connected"));
	p += sprintf(p, "MACAddress=\"%02x:%02x:%02x:%02x:%02x:%02x\"\n",
		     dev->dev_addr[0], dev->dev_addr[1], dev->dev_addr[2],
		     dev->dev_addr[3], dev->dev_addr[4], dev->dev_addr[5]);
	//mc_count = netdev_mc_count(dev);
	p += sprintf(p, "MCCount=\"%d\"\n", dev->mc.count);

	/* Put out the multicast list */
	i = 0;
	netdev_for_each_mc_addr(ha, dev) {
	    p += sprintf(p,
			 "MCAddr[%d]=\"%02x:%02x:%02x:%02x:%02x:%02x\"\n",
			 i,
			 ha->addr[0], ha->addr[1], ha->addr[2],
			 ha->addr[3], ha->addr[4], ha->addr[5]);
	    i++;
	}

	netif_addr_unlock_bh(dev);

	p += sprintf(p, "num_tx_bytes = %lu\n", priv->stats.tx_bytes);
	p += sprintf(p, "num_rx_bytes = %lu\n", priv->stats.rx_bytes);
	p += sprintf(p, "num_tx_pkts = %lu\n", priv->stats.tx_packets);
	p += sprintf(p, "num_rx_pkts = %lu\n", priv->stats.rx_packets);
	p += sprintf(p, "num_tx_pkts_dropped = %lu\n", priv->stats.tx_dropped);
	p += sprintf(p, "num_rx_pkts_dropped = %lu\n", priv->stats.rx_dropped);
	p += sprintf(p, "num_tx_pkts_err = %lu\n", priv->stats.tx_errors);
	p += sprintf(p, "num_rx_pkts_err = %lu\n", priv->stats.rx_errors);
	p += sprintf(p, "num_tx_timeout = %u\n", priv->num_tx_timeout);
	p += sprintf(p, "carrier %s\n",
		     ((netif_carrier_ok(priv->uap_dev.netdev)) ? "on" : "off"));
	p += sprintf(p, "tx queue %s\n",
		     ((netif_queue_stopped(priv->uap_dev.netdev)) ? "stopped" :
		      "started"));

	data->readlen = strlen(data->rbuffer);
	return 0;
}

/**
 *  @brief proc_open_hwstatus
 *
 *  @param inode
 *  @param file
 *  @return 0 or -ERROR
 */
static int proc_open_hwstatus(struct inode *inode, struct file *file)
{
	struct net_device *dev = PDE_DATA(inode);
	uap_private *priv = (uap_private *) netdev_priv(dev);
	struct proc_data *data;

	MODULE_GET;

	if ((file->private_data =
	     kzalloc(sizeof(struct proc_data), GFP_KERNEL)) == NULL) {
		MODULE_PUT;
		return -ENOMEM;
	}
	data = file->private_data;
	if ((data->rbuffer = kmalloc(16, GFP_KERNEL)) == NULL) {
		kfree(file->private_data);
		MODULE_PUT;
		return -ENOMEM;
	}
	if ((data->wbuffer = kmalloc(16, GFP_KERNEL)) == NULL) {
		kfree(data->rbuffer);
		kfree(file->private_data);
		MODULE_PUT;
		return -ENOMEM;
	}
	data->maxwritelen = 16;

	sprintf(data->rbuffer, "%d\n", priv->adapter->HardwareStatus);
	data->readlen = strlen(data->rbuffer);

	MODULE_PUT;
	return 0;
}

static int proc_release_hwstatus(struct inode *inode, struct file *file)
{
	struct proc_data *data = file->private_data;
	struct net_device *dev = PDE_DATA(inode);
	uap_private *priv = (uap_private *) netdev_priv(dev);
	char databuf[10];
	int hwstatus;

	MODULE_GET;
	if (!data->writelen) {
		/* nothing written */
		goto quit;
	}

	if (data->writelen >= sizeof(databuf)) {
		/* overflow */
		goto quit;
	}

        /*
	strncpy(databuf, data->wbuffer, data->writelen);
	databuf[data->writelen] = '\0';
	hwstatus = string_to_number(databuf);
        */
        hwstatus = string_to_number(data->wbuffer);
	switch (hwstatus) {
	case HWReset:
		PRINTM(MSG, "reset hw\n");
		uap_soft_reset(priv);
		//priv->adapter->HardwareStatus = HWReset;
		break;
	default:
		PRINTM(MSG, "Unknown request!\n");
		break;
	}
quit:
	if (data->on_close != NULL) {
		printk("data->on_close");
		data->on_close(inode, file);
        }
	kfree(data->rbuffer);
	kfree(data->wbuffer);
	kfree(data);
	MODULE_PUT;
	return 0;
}

/********************************************************
		Global Functions
********************************************************/
/**
 *  @brief create uap proc file
 *
 *  @param priv	   pointer uap_private
 *  @param dev     pointer net_device
 *  @return 	   N/A
 */
int uap_proc_entry(uap_private * priv, struct net_device *dev)
{
	struct proc_dir_entry *entry;
/*
        char msg[128];

        proc_kuid = make_kuid(&init_user_ns, proc_uid);
        proc_kgid = make_kgid(&init_user_ns, proc_gid);
        if (!uid_valid(proc_kuid) || !gid_valid(proc_kgid))
                return -EINVAL;
*/

	PRINTM(INFO, "Creating Proc Interface\n");
	/* Check if uap directory already exists */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26)
	for (r = r->subdir; r; r = r->next) {
		if (r->namelen && !strcmp("uap", r->name)) {
			/* Directory exists */
			PRINTM(WARN, "proc directory already exists!\n");
			priv->proc_uap = r;
			break;
		}
	}
#endif

/* FIXME
    if (!priv->proc_uap) {
        //priv->proc_uap = proc_mkdir("uap", PROC_DIR);
	//strcpy(priv->proc_name, dev->name);
	//priv->proc_uap = proc_mkdir_mode(priv->proc_name, 0555, uap_entry);
	priv->proc_uap = proc_mkdir_mode("uap", 0555, uap_entry);
        if (!priv->proc_uap)
	    return -ENOMEM;
    }
        else
            atomic_set(&priv->proc_uap->count, 1);
    } else {
        atomic_inc(&priv->proc_uap->count);
    }
*/

        /* /proc/uap */
	if (!priv->proc_uap) {
		priv->proc_uap = proc_mkdir("uap", PROC_DIR);
		if (!priv->proc_uap)
			return -ENOMEM;
	}

        /* /proc/uap/uap? */
	priv->proc_entry = proc_mkdir(dev->name, priv->proc_uap);
	if (priv->proc_entry) {
		//proc_set_user(priv->proc_entry, proc_kuid, proc_kgid);
		entry = proc_create_data("info", 0444,
					 priv->proc_entry, &proc_info_ops, dev);
		if (!entry)
			goto fail;
		//proc_set_user(entry, proc_kuid, proc_kgid);

		entry = proc_create_data("hwstatus", 0644,
					 priv->proc_entry, &proc_hwstatus_ops,
					 dev);
		if (!entry)
			goto fail;
		//proc_set_user(entry, proc_kuid, proc_kgid);
		return 0;

fail:
		/* remove parent (all the proc entries) on one failure ? */
		//remove_proc_subtree(priv->proc_uap, uap_entry);
		remove_proc_entry("uap", PROC_DIR);
	}
	return -ENOMEM;
}

/**
 *  @brief remove proc file
 *
 *  @param priv	   pointer uap_private
 *  @return 	   N/A
 */
void uap_proc_remove(uap_private * priv)
{
/* FIXME
    if (priv->proc_uap) {
        if (priv->proc_entry) {
            remove_proc_entry("info", priv->proc_entry);
            remove_proc_entry("hwstatus", priv->proc_entry);
        }
        remove_proc_entry(priv->uap_dev.netdev->name, priv->proc_uap);
        atomic_dec(&priv->proc_uap->count);
        if (atomic_read(&(priv->proc_uap->count)) == 0)
            remove_proc_entry("uap", PROC_DIR);
    }
*/
	proc_remove(priv->proc_uap);
}


#endif
